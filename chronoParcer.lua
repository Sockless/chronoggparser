socket = require "ssl.https"
json = require "dkjson"



local body, code, headers, status = socket.request("https://api.chrono.gg/sale")
local content = json.decode(body)
local savefilePath = "./gamelog.csv"

local currentTime = os.date("*t")

local dateGotten = string.format([[%s/%s/%s %s:%s]], currentTime.day, currentTime.month,currentTime.year, currentTime.hour, currentTime.min)

function writeGameInfo(file)

	file:write("\n" .. content.name .. "," .. content.sale_price .. "," .. content.currency .. "," .. content.discount .. ",")

	for i=1, #content.platforms do 
		file:write(content.platforms[i] .. " ")
	end

	file:write(",".. dateGotten .."," .. content.steam_url)
	
	print(string.format([[
Name: %s
Sale Price: %s %s
Discount: %s
Steam Link: %s
]], content.name, content.sale_price, content.currency, content.discount, content.steam_url ))

end

function file_exists(name)
	local f=io.open(name, "r")
	if f ~= nil then 
		return true
	else
		return false
	end
end

print (status)
if status ~= "HTTP/1.1 200 OK" then
	error("Error connecting to API, check internet connection and try again")
else
	if  file_exists(savefilePath) == false then
		local savefile = io.open(savefilePath, "w+")
		savefile:write("name, price, currency, discount, platforms,date, steam url")
		writeGameInfo(savefile)
	else
		local savefile = io.open(savefilePath, "a")
		writeGameInfo(savefile)
	end
end

chrono.gg Parser

<h2>Summary</h2>
This project is about creating a parser for the game discount site chrono.gg using it's public api found here: http://api.chrono.gg/sale and exporting the data to a readable foramt, mainly CSV. other formats may come in the future. 

<h2>Prerequisits</h2>
- Lua 5.3.4 (Only working tested version)
- OpenSSL libraries for lua
- luasec (for HTTPS requests)